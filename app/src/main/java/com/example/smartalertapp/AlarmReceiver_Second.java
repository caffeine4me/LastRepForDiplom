package com.example.smartalertapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.net.Uri;
import android.util.Log;

public class AlarmReceiver_Second extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("We are in the Receiver.", "Yay!");

        String get_your_string = intent.getExtras().getString("extra");

        Log.e("What is the key?", get_your_string);

        Intent service_intent = new Intent(context, RingtonePlayingService.class);
        service_intent.putExtra("extra", get_your_string);
        context.startService(service_intent);
    }
}
