package com.example.smartalertapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;


public class AlarmThird extends AppCompatActivity{

    TimePicker timePicker;
    TextView textView;
    Button buttonTime, buttonSet, LedOn, LedOff, TeapotOn, TeapotOff;
    Switch Teapot;
    int mHour, mMin, TeapotInfo = 0;

    private FirebaseAuth mAuth;
    private DatabaseReference uRef;
    private FirebaseAuth.AuthStateListener mAuthListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenu_second_alarm);
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        textView = (TextView) findViewById(R.id.textViewTime);
        buttonTime = (Button) findViewById(R.id.buttonTimeSet);
        LedOn = (Button) findViewById(R.id.LedOn);
        LedOff = (Button) findViewById(R.id.LedOn);
        Teapot = (Switch) findViewById(R.id.switch2);
        TeapotOn = (Button) findViewById(R.id.TeapotOn);
        TeapotOff = (Button) findViewById(R.id.TeapotOff);

        uRef = FirebaseDatabase.getInstance().getReference();


        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                mHour = hourOfDay;
                mMin = minute;
                //textView.setText(textView.getText().toString() + " " + mHour + ":" + mMin);
            }
        });

        Teapot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                TeapotInfo = 1;
            }
        });
    }





    public void setTime (View v)
    {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        int hour = timePicker.getHour();
        int minute = timePicker.getMinute();
        Date date = new Date();
        Calendar cal_alarm = Calendar.getInstance();
        Calendar cal_now = Calendar.getInstance();

        cal_now.setTime(date);
        cal_alarm.setTime(date);

        cal_alarm.set(Calendar.HOUR_OF_DAY, mHour);
        cal_alarm.set(Calendar.MINUTE, mMin);
        cal_alarm.set(Calendar.SECOND, 0);

        if(cal_alarm.before(cal_now))
        {
            cal_alarm.add(Calendar.DATE, 1);
        }

        if(TeapotInfo == 1){
            Intent i = new Intent(this, MyBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 24444, i, 0);
            alarmManager.set(AlarmManager.RTC_WAKEUP, cal_alarm.getTimeInMillis(), pendingIntent);
        }

        else {
            /*AlarmManager alarmManager1 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            int hour1 = timePicker.getHour();
            int minute1 = timePicker.getMinute();
            Date date1 = new Date();
            Calendar cal_alarm1 = Calendar.getInstance();
            Calendar cal_now1 = Calendar.getInstance();

            cal_now1.setTime(date1);
            cal_alarm1.setTime(date1);

            cal_alarm1.set(Calendar.HOUR_OF_DAY, mHour);
            cal_alarm1.set(Calendar.MINUTE, mMin);
            cal_alarm1.set(Calendar.SECOND, 0);

            if(cal_alarm1.before(cal_now1))
            {
                cal_alarm1.add(Calendar.DATE, 1);
            }*/

            Intent i = new Intent(this, MyBrRAll.class);
            PendingIntent pendingIntent1 = PendingIntent.getBroadcast(this, 2444, i, 0);
            alarmManager.set(AlarmManager.RTC_WAKEUP, cal_alarm.getTimeInMillis(), pendingIntent1);
        }







        if(mMin < 10)
        {
            textView.setText(" ");
            textView.setText(textView.getText().toString() + "Будильник установлен на " + mHour + ":0" + mMin);
        } else if (mMin > 10){
            textView.setText(" ");
            textView.setText(textView.getText().toString() + "Будильник установлен на " + mHour + ":" + mMin);
        }

        //setAlarmText("Будильник поставлен на " + hour + ":" + minute);

    }

    public void LedOn (View v) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference mRef = database.getReference("LED_INFO");
            int childValue = 1;
            mRef.setValue(childValue);
        }

    public void LedOff (View v) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference mRef = database.getReference("LED_INFO");
        int childValue = 0;
        mRef.setValue(childValue);
    }

    public void TeapotOn (View v) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference mRef = database.getReference("TEAPOT_INFO");
        int childValue = 1;
        mRef.setValue(childValue);
    }

    public void TeapotOff (View v) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference mRef = database.getReference("TEAPOT_INFO");
        int childValue = 0;
        mRef.setValue(childValue);
    }
}
